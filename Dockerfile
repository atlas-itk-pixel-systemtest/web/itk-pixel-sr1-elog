# sr1-elog-centos7
FROM almalinux:9

# TODO: Put the maintainer name in the image metadata
MAINTAINER Benedikt Vormwald <Benedikt.Vormwald@cern.ch>

# TODO: Rename the builder environment variable to inform users about application you provide them
# ENV BUILDER_VERSION 1.0

# TODO: Set labels used in OpenShift to describe the builder image
LABEL io.k8s.description="Platform for building PSI elog for ITK-Pixel-SR1" \
      io.k8s.display-name="itk-pixel-sr1-elog" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder,itk-pixel-sr1-elog,elog"

ADD ./elog-3.1.5-20240226.el9.x86_64.rpm /root/elog-3.1.5-20240226.el9.x86_64.rpm

# TODO: Install required packages here
RUN dnf install -y rsync psmisc
RUN dnf install -y sendmail sendmail-cf make
RUN dnf install -y epel-release
RUN dnf install -y ImageMagick ImageMagick-devel
RUN dnf install -y emacs-nox ghostscript openssl-devel && dnf clean all -y
RUN dnf install -y /root/elog-3.1.5-20240226.el9.x86_64.rpm
RUN rm /root/elog-3.1.5-20240226.el9.x86_64.rpm

RUN mkdir -p /elog/logbooks
RUN mkdir -p /usr/share/elog

# Set timezone
RUN mv /etc/localtime /etc/localtime.old
RUN ln -s /usr/share/zoneinfo/Europe/Zurich /etc/localtime

# Do sendmail configuration
ADD ./sendmail.mc /etc/mail/sendmail.mc
RUN cd /etc/mail/; make

# Set the default port for applications built using this image
EXPOSE 8080

# Copy the entry point startup command script
ENV HOME /home

COPY ./entrypoint.sh $HOME/entrypoint.sh
COPY ./elogd.cfg /elog/logbooks/elogd.cfg
RUN chmod a+r $HOME/entrypoint.sh

# Set the default ENTRYPOINT and CMD for the image
ENTRYPOINT ["/home/entrypoint.sh"]
