#!/bin/bash

echo "Starting elog"

if [[ -z "${NOACTION}" ]]; then
  COUNT=600
  while [ $COUNT -gt 0 ]; do
    echo $COUNT
    let COUNT=COUNT-1
    sleep 1
  done
  echo "shutting down!"
else
  elogd -p 8080 -c /elog/elogd.cfg
fi
